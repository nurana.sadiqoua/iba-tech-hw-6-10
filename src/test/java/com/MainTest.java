package com;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {

    Family family = new Family();
    Human[] children = new Human[2];
    Human child1 = new Human();
    Human child2 = new Human();

    @Test
    void callAddChildAndExpectFamilySizeToIncrease(){

        int pre = family.getChildren().length;
        family.addChild(child1);
        assertEquals(pre + 1, family.getChildren().length);

    }

    @Test
    void callDeleteChildByObjectAndExpectFamilyToReduceInSizeIfItIsMember(){

        family.addChild(child1);
        boolean isDeleted = family.deleteChild(child1);

        Assertions.assertTrue(isDeleted);
    }

    @Test
    void callDeleteChildAndExpectFamilyToBeSameIfItIsNotMember(){

        family.addChild(child1);
        boolean isDeleted = family.deleteChild(child2);

        Assertions.assertFalse(isDeleted);
    }


    @Test
    void countFamily() {
        family.addChild(child1);
        int c = family.countFamily();

        Assertions.assertTrue(c == 3);
    }
}