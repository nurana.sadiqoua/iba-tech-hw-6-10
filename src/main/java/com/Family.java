package com;

import java.util.Arrays;
import java.util.Objects;

public class Family {
    private Human mother;
    private Human father;
    private Human[] children;
    private Pet pet;

    public Family(Human mother, Human father, Human[] children, Pet pet) {
        this.mother = mother;
        this.father = father;
        this.children = children;
        this.pet = pet;
    }

    public Family() {
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Human[] getChildren() {

        if (children == null) {
            return new Human[0];
        }

        return children;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) && Objects.equals(father, family.father) && Arrays.equals(children, family.children) && Objects.equals(pet, family.pet);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(mother, father, pet);
        result = 31 * result + Arrays.hashCode(children);
        return result;
    }

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", children=" + Arrays.toString(children) +
                ", pet=" + pet +
                '}';
    }

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
    }

    public Human[] addChild(Human child) {
        Human[] newChildren;
        if (children == null) {
            children = new Human[0];
        }

        newChildren = new Human[this.children.length + 1];


        for (int i = 0; i < this.children.length; i++) {
            newChildren[i] = children[i];
        }
        newChildren[this.children.length] = child;

        child.setFamily(this);

        children = newChildren;

        return newChildren;
    }

    public boolean deleteChild(int i) {
        boolean isDeleted = false;
        if (i < children.length) {
            Human[] newChildren = new Human[children.length - 1];
            isDeleted = true;
            for (int j = 0; j < i; j++) {
                newChildren[j] = children[j];
            }
            for (int j = i + 1; j < children.length; j++) {
                newChildren[j - 1] = children[j];
            }
            children[i].getFamily().setChildren(newChildren);
        } else {
            System.out.println("This child is not a member of family");
        }
        return isDeleted;
    }

    public boolean deleteChild(Human child) {
        boolean isDeleted = false;

        int deleteIndex;

        for (int i = 0; i < children.length; i++) {
            if (children[i] == child) {
                deleteIndex = i;
                isDeleted = true;

                deleteChild(deleteIndex);
            }
        }

        if (!isDeleted) {
            System.out.println("This child is not member of family");
        }

        return isDeleted;
    }

    public int countFamily() {
        int count = 2;
        return count += this.children.length;
    }
}