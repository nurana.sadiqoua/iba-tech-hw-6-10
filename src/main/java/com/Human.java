package com;

import java.util.Arrays;
import java.util.Objects;
import java.util.Random;

public class Human {
    private String name;
    private String surName;
    private int year;
    private int iq;
    private String schedule[][];
    private Family family;

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surName='" + surName + '\'' +
                ", year=" + year +
                ", iq=" + iq +
                ", schedule=" + Arrays.deepToString(schedule) +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return year == human.year && iq == human.iq && Objects.equals(name, human.name) && Objects.equals(surName, human.surName) && Arrays.equals(schedule, human.schedule) && Objects.equals(family, human.family);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(name, surName, year, iq, family);
        result = 31 * result + Arrays.hashCode(schedule);
        return result;
    }

    public void greetPet(Family family) {
        System.out.println("Hello, " + family.getPet().getNickName());
    }

    public void describePet(Family family) {
        if (family.getPet().getTrickLevel() > 50) {
            System.out.println("I have a " + family.getPet().getSpecies() + " he is " + family.getPet().getAge()
                    + " years old, he is very sly");
        } else {
            System.out.println("I have a " + family.getPet().getSpecies() + " he is " + family.getPet().getAge()
                    + " years old, he is almost not sly");
        }
    }

    public boolean feedPet(boolean feedTime, Family family) {
        boolean feedHappening = false;
        Random random = new Random();
        int pseudoRandom = random.nextInt(100);
        if (feedTime || family.getPet().getTrickLevel() > pseudoRandom) {
            System.out.println("Hm... I will feed Jack's " + family.getPet().getNickName());
            feedHappening = true;
        } else {
            System.out.println("I think " + family.getPet().getNickName() + " is not hungry.");
        }
        return feedHappening;
    }

    public Human(String name, String surName, Family family) {
        this.name = name;
        this.surName = surName;
        this.family = family;
    }

    public Human(String name, String surName, int year, int iq, String[][] schedule, Family family) {
        this.name = name;
        this.surName = surName;
        this.year = year;
        this.iq = iq;
        this.schedule = schedule;
        this.family = family;
    }

    public Human() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public String[][] getSchedule() {
        return schedule;
    }

    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }
}