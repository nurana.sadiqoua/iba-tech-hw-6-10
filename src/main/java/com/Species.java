package com;

public enum Species {
    DOG,
    CAT,
    PARROT,
    HORSE;
}
