package com;

public class Main {
    public static void main(String[] args) {

        Human mother = new Human();
        Human father = new Human();

        mother.setName("Jane");
        mother.setSurName("Karleone");
        mother.setIq(93);

        father.setName("Vito");
        father.setSurName("Karleone");
        father.setIq(93);

        DayOfWeek monday = DayOfWeek.MONDAY;
        DayOfWeek tuesday = DayOfWeek.TUESDAY;
        String[][] scheduleMother = new String[][]{{monday.name(), "Hang out with friends"},
                {tuesday.name(), "Help to children with homeworks"}};
        mother.setSchedule(scheduleMother);

        DayOfWeek friday = DayOfWeek.FRIDAY;
        DayOfWeek sunday = DayOfWeek.SUNDAY;
        String[][] scheduleFather = new String[][]{{friday.name(), "Go to library"},
                {sunday.name(), "Go to zoo with children"}};
        father.setSchedule(scheduleFather);


        Family karleone = new Family(mother, father);

        Pet pet = new Pet(Species.CAT, "Marta", 3, 75, new String[]{"eat", "drink", "sleep"});
        karleone.setPet(pet);

        Human[] children = new Human[2];
        Human child1 = new Human("Michail", "Karleone", karleone);
        Human child2 = new Human("Sonny", "Karleone", karleone);
        children[0] = child1;
        children[1] = child2;
        karleone.setChildren(children);
        System.out.println(karleone);
        // mother father and children are set to family
        // next methods of the class are checked

        mother.describePet(karleone);
        father.greetPet(karleone);
        child1.feedPet(true, karleone);

        karleone.getPet().eat();
        karleone.getPet().foul();
        karleone.getPet().respond();
        // different members of family describe greet and feed the pet. And we got respond from pet.

        Human child3 = new Human("Constanzia", "Karleone", karleone);
        children = karleone.addChild(child3);
        System.out.println(karleone);

        boolean b = karleone.deleteChild(2);
        System.out.println(karleone);

        Human child4 = new Human("Fredo", "Karleone", karleone);
        children = karleone.addChild(child4);
        System.out.println(karleone);

        karleone.deleteChild(child4);
        System.out.println(karleone);
        // we added and deleted child firstly by index and then as object

        Human child5 = new Human("Tom", "Hagen", karleone);
        karleone.deleteChild(child5);
        System.out.println(karleone);
        karleone.deleteChild(10);
        System.out.println(karleone);
        // we tried to delete child which is not member of family with object and index parametrized methods

        System.out.println(karleone.countFamily());
        // count of family members is printed

        // chilren[2] //child1, child2
        // child3

        // newChildren[3] - - -
        // chilren[2]
        //
    }
}
